import time
import datetime
from celery import shared_task
from .models import Task
import random
@shared_task
def new_task():
    created_at =  datetime.datetime.now(datetime.timezone.utc)
    my_task = Task.objects.create(created_at=created_at)
    my_task.save()
    time.sleep(random.uniform(0., 10.))
    my_task.completed_at = datetime.datetime.now(datetime.datetime.utc)
    my_task.save()