from rest_framework.decorators import api_view
from rest_framework.response import Response
from .task import new_task

@api_view(["GET"])
def task(request, *args, **kwarg):
    new_task.delay()
    return Response(status=204)