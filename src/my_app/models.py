from django.db import models

# Create your models here.
class Task(models.Model):
    created_at = models.DateTimeField(null=True)
    completed_at = models.DateTimeField(null=True)