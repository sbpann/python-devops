#!/bin/sh

cd src
mv .env .env_
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py test
mv .env_ .env
