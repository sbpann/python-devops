#!/bin/sh
if [ $1 = 'dev' ];
then 
    docker build -t registry.gitlab.com/sbpann/python-devops:dev -f ./dockerfiles/dev.Dockerfile ./src
else
    docker build -t registry.gitlab.com/sbpann/python-devops -f ./dockerfiles/prod.Dockerfile ./src
    docker push registry.gitlab.com/sbpann/python-devops
fi
