from django.test import TestCase
from .models import Task
import datetime
class TaskTestCase(TestCase):
    def setUp(self):
        Task.objects.create(created_at=datetime.datetime.now(datetime.timezone.utc))

    def test_create_success(self):
        my_task = Task.objects.get(id="1")
        self.assertIsNotNone(my_task)
        self.assertIsNotNone(my_task.created_at)
        self.assertIsNone(my_task.completed_at)
        my_task.completed_at = datetime.datetime.now(datetime.timezone.utc)
        my_task.save()
        self.assertIsNotNone(my_task.completed_at)